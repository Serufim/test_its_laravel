
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import VueRouter from 'vue-router';

window.Vue.use(VueRouter);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import ExampleComponent from './components/ExampleComponent.vue';
import CreateComponent from './components/CreateComponent.vue';
import EditComponent from './components/EditComponent.vue';
const routes = [
    {path: '/', component:ExampleComponent},
    {path: '/create', component: CreateComponent, name: 'createPost'},
    {path: '/edit/:id', component: EditComponent, name: 'editComponent'},
];
const router = new VueRouter({ routes });
const app = new Vue({
    router:router,
    el: '#app',
});
