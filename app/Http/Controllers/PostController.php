<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Http\Resources\PostResource;
class PostController extends Controller
{
    public $validate_rules = [
        'title' => 'required|unique:posts|max:255',
        'content' => 'required',
        'author' => 'required|max:255',
        'reader' => 'required|max:255',
    ];

    public function index()
    {
        return PostResource::collection(Post::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate($this->validate_rules);
        try{
        $post = Post::create([
            'title' => $request->title,
            'content' => $request->content,
            'author' => $request->author,
            'reader' => $request->reader,
        ]);
        }catch (\Exception $e){
            return "У тебя ничего не вышло";
        }

        return new PostResource($post);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return new PostResource($post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $own_validate_rules = $this->validate_rules;
        $own_validate_rules['title'] = 'required|max:255|unique:posts,id,'.$request->id;
        $validatedData = $request->validate($own_validate_rules);
        try {
            $post->update($request->all());
        } catch (\Exception $e) {
            return "У тебя ничего не вышло";
        }
        return new PostResource($post);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->delete();
        return response()->json(null,204);
    }
}
